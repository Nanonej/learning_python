#!/usr/local/bin/python3.4

def is_bissextile(n):
	if n % 4 == 0:
		if n % 100 == 0 and n % 400 == 0:
			return True
		elif n % 100 != 0:
			return True
	return False

n = input("Enter a year : ")
n = int(n)
print(is_bissextile(n))
