#!/usr/local/bin/python3.4

from socket import *

host = 'localhost'
port = 42335

connection_with_serv = socket(AF_INET, SOCK_STREAM)
connection_with_serv.connect((host, port))
print("Connection established with the server on the port {}".format(port))
msg_send = b""
while (msg_send != b"end"):
	msg_send = input("> ")
	msg_send = msg_send.encode()
	connection_with_serv.send(msg_send)
	msg_recv = connection_with_serv.recv(1024)
	print(msg_recv.decode())
print("closing connection")
connection_with_serv.close()
