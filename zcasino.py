#!/usr/local/bin/python3.4

from random import randrange
from math import ceil

def is_pair(n):
	if (n % 2 == 0):
		return (True)
	return (False)

def zcasino(n, mise):
	rand = randrange(50)
	if (is_pair(n) and is_pair(rand)):
		if (n == rand):
			mise += 3 * mise
			return (mise)
		mise += mise / 2
		return (ceil(mise))
	return (0)

n = input("Enter a number : ")
mise = input("Enter a mise : ")
n = int(n)
mise = int(mise)
print(zcasino(n, mise))
