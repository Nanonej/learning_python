#!/usr/local/bin/python3.4

from socket import *

host = ''
port = 42335

main_connection = socket(AF_INET, SOCK_STREAM)
main_connection.bind((host, port))
main_connection.listen(5)
print("The server listen now on the port {}".format(port))
connection_with_client, infos_connection = main_connection.accept()
msg_recv = b""
while (msg_recv != b"end"):
	msg_recv = connection_with_client.recv(1024)
	print(msg_recv.decode())
	connection_with_client.send(b"OK")
print("closing connection")
connection_with_client.close()
main_connection.close()
